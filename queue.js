let collection = [];


// Write the queue functions below.
	

// 1. Output all the elements of the queue
	function print() {
		return collection
	}


// 2. Adds element to the rear of the queue
function enqueue(text){
	collection.push(text);
	return collection
}


// 3. Removes element from the front of the queue
function dequeue(text){
	collection.shift(text)
	return collection
}


// 4. Show element at the front

function front(){	
	return collection[0]

}


// 5. Show the total number of elements
function size(){
	return collection.length
}


// 6. Outputs a Boolean value describing whether queue is empty or not

function isEmpty(){
	if(collection == ""){
		return true
	}else{
		return false
	}
}


module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};
